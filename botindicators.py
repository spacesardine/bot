import numpy
import talib
#https://www.ta-lib.org

#add ULTOSC and MFI

class BotIndicators(object):
	def __init__(self):
		pass

	def SMA(data):
		priceArray = numpy.asarray(data)
		sma = talib.SMA(priceArray)
		return sma

	def EMA(data):
		priceArray = numpy.asarray(data)
		ema = talib.EMA(priceArray)
		return ema

	def BBANDS(data):
		priceArray = numpy.asarray(data)
		upper, middle, lower = talib.BBANDS(priceArray)
		return upper, middle, lower

	def STOCHRSI(data):
		priceArray = numpy.asarray(data)
		rsi1, rsi2 = talib.STOCHRSI(priceArray)
		return rsi1, rsi2

	def RSI(data):
		priceArray = numpy.asarray(data)
		rsi1, rsi2 = talib.RSI(priceArray)
		return rsi1, rsi2

	def MACD(data, fastperiod=12, slowperiod=26, signalperiod=9):
		priceArray = numpy.asarray(data)
		slow, fast, histo = talib.MACD(priceArray,
										fastperiod=fastperiod,
										slowperiod=slowperiod,
										signalperiod=signalperiod)
		return slow, fast, histo

	def MINMAX(data):
		priceArray = numpy.asarray(data)
		mini, maxi = talib.MINMAX(priceArray)
		return mini, maxi
