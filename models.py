

class Profile(object):
    """
        Represents entire profile of a user
    """
    
    def __init__(self, profile_id, email_id):
        self.profile_id = profile_id
        self.email_id = email_id

    
    def set_discord_profile(self, discord_id, discord_name):
        self.discord_id = discord_id
        self.discord_name = discord_name


class Alerts(object):
    
    def __init__(self, signals, notifyVia, earlyNotify ):
        self.signals = signals
        self.notifyVia = notifyVia
        self.earlyNotify = earlyNotify

    