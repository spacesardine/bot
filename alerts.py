#i dont know if all of the .keys() usage in the for loops was needed
#you many see fileIO() but that is just a fucntion i used to use for writing json files
#feel free to change whatever, i coded this most of this without testing it

import json
import time
import os
import smtplib
import schedule


from binance.client import Client

#bitmex imports, copied from the Sample Market Maker Bot, no idea what imports are needed or not
#https://github.com/BitMEX/sample-market-maker
from market_maker import bitmex 
from market_maker.utils import log, constants, errors, math

#custom gmail notification script
import send_mail


#needed for send_mail to send gmail emails
service_email = "your email"
service_password = "your password"

binance_api_key = os.environ["BIN_KEY"]
binance_api_secret = os.environ["BIN_SECRET"]

binance_client = Client(binance_api_key, binance_api_secret)



class CryptoAlerts(object):
    def __init__(self, config_file_address):
        config_file_address = os.path.abspath(config_file_address)
        self.config_file_address = config_file_address
        with open(self.config_file_address, "r") as config_file:
            self.config = json.load(config_file)

    def __str__(self):
        return f"""Config address \N{rightwards arrow} {self.config_file_address}
                        Config data \N{rightwards arrow} {self.config}"""

    def get_config_file(self):
        return self.config

    def write_config_file(self, json_data):
        try:
            with open(self.config_file_address, "w") as config_file:
                json.dump(json_data, config_file, indent=4)
            return 0
        except Exception as exception:
            print(exception)
            return 1

    #build unique list of signals, markets and timeframes from profiles.json
    def buildSignalsList(self, profiles): 

        #this var will be the unique list of signals, markets and timeframes to check for 
        #it also stores user conditions for where and when to send notifications
        signalList = {}

        #loop through profiles file to build unique list of signals and user alert preferences
        for user in profiles.keys():

            #'signalAlerts' consist of RSI, STOCHRSI, MACD, SMA-Crosses and Simple price crossings
            for signal in user['signalAlerts'].keys():

                if signal == "RSI":              
                    for market in signal["markets"].keys():
                        for tf in ["timeFrames"].keys():
                        #check if this user wants a notification
                            if signal in user['signalAlerts'].keys():
                                if market in user['signalAlerts'][signal].keys():
                                    if tf in user['signalAlerts'][signal][market].keys():
                                        #if someone wants notifications for this, only then add it to the signalList
                                        if 'up' or 'down' in tf["signals"].values() 
                                                and 'email' or 'discord' in tf["notifyVia"].values(): 
                                            #store list of users + notification data in signalList
                                            signalList[signal][market][tf][user] = {"signals":tf["signals"], "notifyVia": tf["notifyVia"]}
                                        
                elif signal == "STOCHRSI":
                    for market in signal["markets"].keys():
                        for tf in ["timeFrames"].keys():  
                            if signal in user['signalAlerts'].keys():
                                if market in user['signalAlerts'][signal].keys():
                                    if tf in user['signalAlerts'][signal][market].keys():
                                        if 'up' or 'down' in tf["signals"].values() 
                                                and 'email' or 'discord' in tf["notifyVia"].values():   
                                            signalList[signal][market][tf][user] = {"signals":tf["signals"], "notifyVia": tf["notifyVia"]}

                #this only differs from the ones above in that it has a 'histo' option in tf["signals"].values()
                #this is the MACD Histogram, it has a value either above or below 0 and we are interested in when it crosses 0
                elif signal == "MACD":
                    for market in signal["markets"].keys():
                        for tf in ["timeFrames"].keys():  
                            if signal in user['signalAlerts'].keys():
                                if market in user['signalAlerts'][signal].keys():
                                    if tf in user['signalAlerts'][signal][market].keys():
                                        if 'up' or 'down' or 'histo' in tf["signals"].values() 
                                                and 'email' or 'discord' in tf["notifyVia"]:   
                                            signalList[signal][market][tf][user] = {"signals":tf["signals"], "notifyVia": tf["notifyVia"]}

                #i added the 'lengths' parameter to the profiles structure to accomodate multiple sma cross pairs
                #in the profiles.json file you will see that in the SMA-Cross section, under the timeframes of 1h, 2h, 4h, you will see 13_34 and 50_100
                #these correspond to SMA lengths, i used the underscore to delineate them
                #the smaller the number (of closing prices used to calculate it), the faster the SMA is considered to be
                elif signal == "SMA-Cross":
                    for market in signal["markets"].keys():
                        for tf in market["timeFrames"].keys():  
                            #each length is two numbers like '13_34' or '50_100' (single digits may be used, but 4+ digits will not)
                            for length in tf.keys():
                                if signal in user['signalAlerts'].keys():
                                    if market in user['signalAlerts'][signal].keys():
                                        if tf in user['signalAlerts'][signal][market].keys():
                                            if length in user['signalAlerts'][signal][market][tf].keys(): 
                                                if 'up' or 'down' in length["signals"].values() 
                                                        and 'email' or 'discord' in length["notifyVia"].values():   
                                                    signalList[signal][market][tf][length][user] = {"signals":length["signals"], "notifyVia": length["notifyVia"]}
                
                #simple price cross alerts
                #this one differs only in that the var 'tf' is changed to 'price'
                #these are independent of timeframe
                elif signal == "PriceAlerts":
                    for market in signal["markets"].keys():
                        for price in ["prices"].keys():  
                            if signal in user['signalAlerts'].keys():
                                if market in user['signalAlerts'][signal].keys():
                                    if price in user['signalAlerts'][signal][market].keys():
                                        if 'up' or 'down' in price["signals"].values() 
                                                and 'email' or 'discord' in price["notifyVia"].values():   
                                            signalList[signal][market][price][user] = {"signals":price["signals"], "notifyVia": price["notifyVia"]}





            #these "frequencyAlerts" notifications go out on a set schedule
            #but should be able to be sent early if the user has set a custom offset in their profile
            #in the last line of this block, it says "earlyNotify", this would be the custom early reminder offset to accomodate. it can be in minutes, so for example "earlyNotify": 5 == 5 minutes
            for timeSignal in user["frequencyAlerts"].keys():
                #if this is a Stock Exchange or Candle signal AND if the user wants a notification
                if 'open' or 'close' or True in timeSignal["signals"].values():
                        and 'email' or 'discord' in timeSignal["notifyVia"].values():   
                    signalList["frequencyAlerts"][timeSignal][market][user] = {"earlyNotify":timeSignal["earlyNotify"], "signals":timeSignal["signals"], "notifyVia": timeSignal["notifyVia"]}
                
               #BITMEX Funding happens at fixed intervals, this adds to signalList, all the people who have 'mexFunding' in their profile
                elif 'XBTUSD' or 'ETHUSD' in timeSignal.keys():
                    if 'long' or 'short' in market["signals"].values()
                            and 'email' or 'discord' in timeSignal["notifyVia"].values():   

                        #the timeSignal["threshold"] variable is a custom user set parameter
                        #eg. if funding == -0.3 or +0.3, and the user has ['signals']: 'long' or 'short', threshold: 0.4, no notification will be generated
                        
                        #but if the funding was +0.5 and the ['signal'] == 'short', a notification would be generated
                        #if the funding was -0.5 and the ['signal'] == 'long', a notification would be generated
                        #when funding is negative, shorts pay longs, and thus 'long' is someone interested in this situation
                        #feel free to ask me about this as much as needed

                        #store list of users + notification data in signalList
                        signalList["frequencyAlerts"][timeSignal][market][user] = {"threshold":timeSignal["threshold"], "earlyNotify":timeSignal["earlyNotify"], "signals":timeSignal["signals"], "notifyVia": timeSignal["notifyVia"]}




            #this adds the people to the list that want notifications for when new markets are added
            #We always check for new markets (well thats what i need you to build)
            #But we also want to know who wants notifications and where they want them
            for exchange in user["NewMarkets"].keys():
                if exchange == "binance":
                    for market in exchange["markets"].values(): 
                        if signal in user['signalAlerts'].keys():
                           if market in user['signalAlerts'][signal].keys():
                                if price in user['signalAlerts'][signal][market].keys():
                                    if 'BTC' or 'ETH' or 'BNB' or 'USDT' in exchange["markets"].values() 
                                            and 'email' or 'discord' in market["notifyVia"].values():   
                                        signalList["NewMarkets"][exchange][market][user] = {"markets":exchange["markets"], "notifyVia": exchange["notifyVia"]}

                elif exchange == "bitmex":
                    for market in exchange["markets"].values(): # values() because it is an array
                        if signal in user['signalAlerts'].keys():
                           if market in user['signalAlerts'][signal].keys():
                                if price in user['signalAlerts'][signal][market].keys():
                                    if 'BTC' or 'alts' in exchange["markets"].values() 
                                            and 'email' or 'discord' in market["notifyVia"].values():   
                                        signalList["NewMarkets"][exchange][market][user] = {"markets":exchange["markets"], "notifyVia": exchange["notifyVia"]}


        return signalsList



       
    def checkForAllSignalsandNotify(self, signalList):

        message = None
        subject = None
        #these are the UTC opening and closing time for the largest stock markets 
        #i didnt know how to store the times properly, so UTC 00:00 == 0  and UTC 08:00 == 800, for example
        #also note we are looping though signalList
        #account for an offset/early notification parameter for all of these 'frequencyAlerts'

        for timeSignal in signalList["frequencyAlerts"].keys():
            if timeSignal == "NYSE-open":
                utcOpen=1430
                utcClose=2100
                for user in timeSignal.keys():
                    #i havent defined 'timeNow' anywhere... its just the current time
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #i didnt make anything to creat the messages yet
                        #message = 'NYSE Opens in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "TSE-Korea-open":
                utcOpen=0
                utcClose=600
                for user in timeSignal.keys():
                   if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = 'Tokyo and Korean Exchanges Opening in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "HKEX-open":
                utcOpen=115
                utcClose=800
                for user in timeSignal.keys():
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = "Hong Kong exchange opens in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "SSE-SZSE-open":
                utcOpen=130
                utcClose=700
                for user in timeSignal.keys():
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = 'Shenzhen and Shanghai exchanges open in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "NASDAQ-open":
                utcOpen=1430
                utcClose=2100
                for user in timeSignal.keys():
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = 'NASDAQ opens in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "LSE":
                utcOpen=800
                utcClose=1630
                for user in timeSignal.keys():
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = 'London Stock Exchange Opens in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)

            elif timeSignal == "INDIA":
                utcOpen=345
                utcClose=1000
                for user in timeSignal.keys():
                    if timeNow + timeSignal["earlyNotify"] >= utcOpen:
                        #message = 'Indian Stock Exchanges open in X minutes'
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    elif timeNow + timeSignal["earlyNotify"] >= utcClose:
                        alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    

    ########if easy, make it so that if there is more than one of these occuring at the same time,
            #that you make a combination message like, 'A new Monthly, Weekly, Daily and 4 Hour Candle all Open in X minutes'
            #else just make it so that only the message for the one with the largest span is sent. 
            #eg. Just send 'New Monthly', rather than all of the cooccuring ones as well

            elif timeSignal == "new4hourCandle":  
                #notify at UTC 0000 or 0400 or 0800 or 1200 or 1600 or 2000
                for user in timeSignal.keys():
                    alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    #message = 'A New 4 Hour Candle Opens in X minutes'

            elif timeSignal == "newDailyCandle":
                #notify @ 0000 UTC 
                #account for an offset/early notification parameter
                for user in timeSignal.keys():
                    alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    #message = 'A New Daily Candle Opens in X minutes'

            elif timeSignal == "newWeeklyCandle":
                #notify on Mondays @ 0000 UTC 
                #account for an offset/early notification parameter
                for user in timeSignal.keys():
                    alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    #message = 'A New Weekly Candle Opens in X minutes'

            elif timeSignal == "newMonthlyCandle":
                #notify on the First Day of Every Month @ 0000 UTC 
                #account for an offset/early notification parameter
                for user in timeSignal.keys():
                    alerts.notifyUser(user, timeSignal["notifyVia"], message)
                    #message = 'A New Monthly Candle Opens in X minutes'




            #BITMEX FUNDING rate signals
            #they happen at set 8 hour intervals of UTC 1200 - UTC 2000 and UTC 0400
            #but usually there is a slight delay before the new rate is available (under 20 seconds delay)
            elif timeSignal == 'mexFunding':
                json = bitmex.instruments()
    ########### you can check here to see if there are any new markets on BitMEX, i think bitmex.instruments() returns every active market

                for i in json:
                    #get current XBTUSD funding rate
                    #you can confirm this data using bitmex.com, i can show you where it displays the perpetual swap funding rates
                    if json[i]["symbol"] == "XBTUSD":
                        #get funding rate info
                        exp = json[i]["fundingTimestamp"][11:15]
                        now = json[i]["timestamp"][11:15] #need to verify if this is the current time or not
                        hoursLeft = int(exp[:1]) - int(now[:1])
                        minsLeft = 60 - int(now[3:4])
                        timeLeftExpiry = hoursLeft + ":" + minsLeft
                        funding = json[i]["fundingRate"]*100 # *100 because we want to display as a percentage

                        message = 'BitMEX XBTUSD Funding Rate is: %' + funding + '\nThe time left to expiry is' + timeLeftExpiry
                        #notifyUsers
                        for user in signalList['frequencyAlerts'][timeSignal][json[i]["symbol"]]:
                            if 'long' in user['signals'].keys():
                                if funding >= user['threshold']:
                                    alerts.notifyUser(user, user['notifyVia'], message)
                            if 'short' in user['signals'].keys():
                                if funding <= user['threshold']:
                                    alerts.notifyUser(user, user['notifyVia'], message)


                    #get current ETHUSD perpetual swap funding rate
                    elif json[i]["symbol"] == "ETHUSD":
                        exp = json[i]["fundingTimestamp"][11:15]
                        now = json[i]["timestamp"][11:15]
                        hoursLeft = int(exp[:1]) - int(now[:1])
                        minsLeft = 60 - int(now[3:4])
                        timeLeftExpiry = hoursLeft + ":" + minsLeft
                        funding = json[i]["fundingRate"]*100

                        message = 'BitMEX ETHUSD Funding Rate is: %' + funding + '\nThe time left to expiry is' + timeLeftExpiry
                        #notifyUsers
                        for user in signalList['frequencyAlerts'][timeSignal][json[i]["symbol"]]:
                            if 'long' in user['signals'].keys():
                                if funding >= user['threshold']:
                                    alerts.notifyUser(user, user['notifyVia'], message)
                            if 'short' in user['signals'].keys():
                                if funding <= user['threshold']:
                                    alerts.notifyUser(user, user['notifyVia'], message)



    #
    #END of alerts that happen at fixed times aka 'frequencyAlerts'
    #

        #"signalAlerts"
        #This Checks for STOCHRSI, RSI, MACD Signals, SMA Crosses and Simple Price Crosses
        for signal in signalList["signalAlerts"].keys():
            if signal == 'SMA-Cross': 
                #this gets new SMA values for each time frame and then checks to see if signal notification conditions have been met
                for market in signal.keys():
                    for tf in market.keys():
                        #get an SMA value for each of two lengths the user can specify
                        #['length'] correspond to SMA lengths, i used the underscore to delineate them, eg. 50_100, 13_34
                        #the smaller the number (of closing prices used to calculate it), the faster the SMA is considered to be
                        for length in signal[market][tf].keys():
                            fast, slow = length.split('_')
                                            
    #####################   save SMA value for comparison (probably have to check if there is one or not, before trying to save it [it would also be cool if it could calculate the previous SMA value if we dont have it already])
                            #this should probably be saved to a file for functions like RSI where we might need more than 2 datapoints to confirm that a signal condition has been met
                            prevSMA[market][tf][fast] = SMA[market][tf][fast] 
                            prevSMA[market][tf][slow] = SMA[market][tf][slow] 

                            #the closeArray needs to be a minimum amount of candles
                            #in this case 'fast' is the int that determines how many candles we need
                            SMA[market][tf][fast] = ind.SMA(closeArray[market][tf], fast)
                            #but here, 'slow' determines the amount of elements the array needs
                            SMA[market][tf][slow] = ind.SMA(closeArray[market][tf], slow)


                            #Crossing Up
                            if (SMA[market][tf][fast] > SMA[market][tf][slow]) & (prevSMA[market][tf][fast] <= prevSMA[market][tf][slow]):
    ########################    idk how to format this for both discord and email format notifications. Email needs a subject line, discord doesnt
                                message = fast + "SMA has crossed above the " + slow + " SMA\n" + market + tf + ' Candles @' + time #make this their local time from their profile
                                if 'up' in length[user]['signals'].keys():
                                    alerts.notifyUser(user, length[user]['notifyVia'], message) 
                            #Crossing Down
                            elif (SMA[market][tf][fast] < SMA[market][tf][slow]) & (prevSMA[market][tf][fast] >= prevSMA[market][tf][slow]):
                                message = fast + "SMA has crossed below the " + slow + " SMA\n" + market + tf + ' Candles @' + time #make this their local time from their profile
                                if 'down' in length[user]['signals'].keys():
                                    alerts.notifyUser(user, length[user]['notifyVia'], message) 


            elif signal == 'RSI': 
                for market in signal.keys():
                    for tf in market.keys():  
                        #this gets and formats RSI overbought and oversold variables from the user profiles
                        #RSI and STOCHRSI tf.keys() only ever == '20_80' or '30_70'
                        #so we split it into the appropriate variables, ob = over bought, os = over sold
                        for param in tf.keys():
                            os, ob = param.split('_')
                            os = int(os)
                            ob = int(ob)
                            
                        #save RSI value for comparison to test if it crosses the desired value
                        prevRSI1[market][tf] = RSI1[market][tf]
                        prevRSI2[market][tf] = RSI2[market][tf]

    ################    # this needs minimum 14 close prices, there should be a function that checks for that before trying to use this function
                        RSI1[market][tf], RSI2[market][tf] = ind.RSI(closeArray[market][tf], 14)

                        #With SMA crossing, the algorithm only needs to compare to the last values
                        #but with 2 RSI values, i think that one of the values might be able to be over 80 for a whole candle period, before the second value crosses over 80
                        #this would stop the alert from triggering because it would have happened over more than two periods
                        #im not sure im correct here, but it might be doable with only 2 datapoints
                        #Crossing Up
                        if (RSI1[market][tf] & RSI2[market][tf] > ob) & (prevRSI1[market][tf] & prevRSI2[market][tf] <= ob):
                            message = market +"RSI is Over Bought @ "+ ob +" on the "+tf+" timeframe.\n"+time
                            if 'up' in param[user]['signals'].keys():
                                    alerts.notifyUser(user, param[user]['notifyVia'], message) 
                        #Crossing Down
                        elif (RSI1[market][tf] & RSI2[market][tf] < os) & (prevRSI1[market][tf] & prevRSI2[market][tf] >= os):
                            message = market +"RSI is Over Sold @ "+ os +" on the "+tf+" timeframe.\n"+time
                            if 'down' in param[user]['signals'].keys():
                                    alerts.notifyUser(user, param[user]['notifyVia'], message) 





            elif signal == 'STOCHRSI': 
                for market in signal.keys():
                    for tf in market.keys():  
                        #get and format RSI overbough and oversold variables from the user profiles
                        #RSI and STOCHRSI tf.keys() only ever == '20_80' or '30_70'
                        #split it into the appropriate variables, ob = over bought, os = oversold
                        for param in tf.keys():
                            os, ob = param.split('_')
                            os = float(os)/100.00 #STOCHRSI() returns values under 1, so 0.8 corresponds to 80 in regular RSI terms
                            ob = float(ob)/100.00
                            
                        #save STOCHRSI value for comparison to test if it crosses the desired value
                        prevSRSI1[market][tf] = SRSI1[market][tf]
                        prevSRSI2[market][tf] = SRSI2[market][tf]

                        #this needs minimum 20 close prices
                        SRSI1[market][tf], SRSI2[market][tf] = ind.STOCHRSI(closeArray[market][tf], 20)

                        #Crossing Up
                        if (SRSI1[market][tf] & SRSI2[market][tf] > ob) & (prevSRSI1[market][tf] & prevSRSI2[market][tf] <= ob):
                            message = market +"STOCH RSI is Over Bought @ "+ ob +" on the "+tf+" timeframe.\n"+time
                            if 'up' in param[user]['signals'].keys():
                                    alerts.notifyUser(user, param[user]['notifyVia'], message)                     
                        #Crossing Down
                        elif (SRSI1[market][tf] & SRSI2[market][tf] < os) & (prevSRSI1[market][tf] & prevSRSI2[market][tf] >= os):
                            message = market +"STOCH RSI is Over Sold @ "+ os +" on the "+tf+" timeframe.\n"+time
                            if 'down' in param[user]['signals'].keys():
                                    alerts.notifyUser(user, param[user]['notifyVia'], message) 





            elif signal == 'MACD': 
                for tf in signal.keys():

                        #save MACD value for comparison
                        prevFastMACD[market][tf] = fastMACD[market][tf]
                        prevSlowMACD[market][tf] = slowMACD[market][tf]
                        #this var is added to account for the extra data the MACD function returns
                        #this is the MACD histogram signal, the signal is when it crosses above or below 0
                        prevHistoMACD[market][tf] = histoMACD[market][tf]

                        #get new MACD values
                        #this needs a minimum of 26 closing prices in the closeArray
                        slowMACD[market][tf], fastMACD[market][tf], histoMACD[market][tf] = ind.MACD(closeArray[market][tf], fastperiod=12, slowperiod=26, signalperiod=9)

                        #Crossing Up
                        if (fastMACD[market][tf] > slowMACD[market][tf]) & (prevFastMACD[market][tf] <= prevSlowMACD[market][tf]):
                            message = "MACD Signal Line has Crossed Up\n"+ market + tf + ' Candles @' + time #make this their local time from their profile
                            if 'up' in tf[user]['signals'].keys():
                                alerts.notifyUser(user, tf[user]['notifyVia'], message)                     
                        #Crossing Down
                        if (fastMACD[market][tf] < slowMACD[market][tf]) & (prevFastMACD[market][tf] >= prevSlowMACD[market][tf]):
                            message = "MACD Signal Line has Crossed Down\n"+ market + tf + ' Candles @' + time #make this their local time from their profile
                            if 'down' in tf[user]['signals'].keys():
                                alerts.notifyUser(user, tf[user]['notifyVia'], message) 

                        #Compare MACD histo values
                        #If either of these conditions are met, the same group of people are sent a notification
                        #Crossing Up
                        if (histoMACD[market][tf] > 0) & (prevHistoMACD[market][tf] <= 0):
                            message = "MACD Histogram has Crossed Up\n"+ market + tf + ' Candles @' + time #make this their local time from their profile
                            if 'histo' in tf[user]['signals'].keys():
                                alerts.notifyUser(user, tf[user]['notifyVia'], message)                       
                        #Crossing Down
                        elif (histoMACD[market][tf] < 0) & (prevHistoMACD[market][tf] >= 0):
                            message = "MACD Histogram has Crossed Down\n"+ market + tf + ' Candles @' + time #make this their local time from their profile
                            if 'histo' in tf[user]['signals'].keys():
                                alerts.notifyUser(user, tf[user]['notifyVia'], message) 



            #These Price Alerts are always one time notifications, once a user is sent a notification, they are always taken off the signalList
            #simple price notifications
            elif signal == "PriceAlerts":
                for market in signal.keys():
                    for price in market.keys():

                        if 'up' or 'down' in price['signals'].values():
                            for direction in price['signals'].values():
                            #this is the user set price
                            desired_price = float(price)
                            #get latest price --- or the current candle high
                            #here you can search for the methods 'get_klines' and 'get_historical_klines' and you can easily extract the high or low of the current candle from those
                            #https://github.com/sammchardy/python-binance/blob/master/binance/client.py

                            #this needs to be able to detect if the desired price was crossed
                            #but it needs to be able to detect in recent history if it quickly went above or below the desired price
                            #but then quickly reverted. If we just check the current price, we could miss when a quick spike happens
                            #i think this is remedied by using the high and low of the current candle, rather than the current price
                            if direction == 'up':
                                current_high_or_low = float(binance_client.get_klines(symbol=market)[0][2]) #get the high
                            elif direction == 'down':
                                current_high_or_low = float(binance_client.get_klines(symbol=market)[0][3]) #get the low
                            #latest_price = float(binance_client.get_symbol_ticker(symbol=market)["price"]) #how to get the latest price of a symbol
                            
    ####################### this algorithm needs some work, im not sure if this will be able to detect when the price crosses the desired price
                            if current_high_or_low >= desired_price: 
                                message = market["name"] + 'has gone above ' + desired_price + ' @' + time
                                alerts.alerts.notifyUser(user, user['notifyVia'], message)
    ########################### on successful notification, delete this from the user profile
                            if current_high_or_low <= desired_price:
                                message = market["name"] + 'has gone below ' + desired_price + ' @' + time
                                alerts.alerts.notifyUser(user, user['notifyVia'], message)
                                #on successful notification, delete this from the user profile
    


    def checkForNewMarkets(self, signalList):
    #this should use the websocket data to saee if there are any markets that have been added since the last check
    #im unsure if you need to maintain a file/list of active markets for each exchange
    #NOTIFICATION message = 'New Market' + newMarket + 'Added to ' +exchange +' @' + time
    #this should get all binance and bitmex active markets and compare them to a list saved from the past to see if there are any new ones or removed ones

                           


    def notifyUser(self, user, platform, message):

        if platform == 'discord':  
            #I don't know how to call the discord bot from here
            #so ive just been putting the messages into a file and have the discord bot check that file a lot...
            
            #load discord messages file
            discord = fileIO('data/discordMSGS.json', "load")
            discord[user] = message

            #write file with new user message (i used to use 'portalocker' so there would be no write collisions but thats just becuase idk how to properly manage data)
            fileIO('data/discordMSGS.json', "save")


        elif platform == 'email':
            #if its an email, then break message into subject and body
            try:
                #copy the first part of the message up to the '\n' and make it the subject
                subject, body = message.split('\n')
            
            #if theres no '\n', use the message as subject too 
            except:
                subject = message
                body = message

            send_mail(subject=subject, msg=body, receiver=user['email-ID'])



def main():

    #load class at top of file
    #load profiles.json
    alerts = CryptoAlerts("profiles.json")
    profiles = alerts.get_config_file()

    #build unique list of signals from user profiles
    signalList = alerts.buildSignalsList(profiles)
    #check if there have been new markets added, if yes, notify interested users
    alerts.checkForNewMarkets(signalList)
    #chack if there are any signals to send out, if yes, notify interested users
    alerts.checkForAllSignalsandNotify(signalList)



#################################################

if __name__ == '__main__':
    schedule.every(10).seconds.do(main)

    while True:
        schedule.run_pending()
        time.sleep(1)