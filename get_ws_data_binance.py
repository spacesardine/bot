import os
import sys
import json

from threading import Timer

from binance.client import Client
from binance.websockets import BinanceSocketManager


binance_client = Client(os.environ["BIN_KEY"], os.environ["BIN_SECRET"])


#here you can search for the methods 'get_klines' and 'get_historical_klines' 
#https://github.com/sammchardy/python-binance/blob/master/binance/client.py

#binance websocket docs require this to be used and they offer specs for it
#i think it recieves the WS data and then we pass the data to the database
def call_back(message):
    """
    The function that gets called back by Binance's sockets.
    This function receives the data from Binance.

    It find the symbol, and relevent interval, and inserts data.
    """
    #example of 
    {
    message["s"]:{ #symbol
        message["k"]["i"]:{ #interval/timeframe
            int(message["k"]["t"]): float(message["k"]["c"]) #time and close price
            }
        }
    }
    
    print(message["s"], message["k"]["i"], message["k"]["c"]) 
    

# This is a function i figured you may want the docs for to figure out which data we need to get with the call_back function

"""
def start_kline_socket(self, symbol, callback, interval=Client.KLINE_INTERVAL_1MINUTE):
    Start a websocket for symbol kline data
    :param callback: callback function to handle messages
    :param interval: Kline interval, default KLINE_INTERVAL_1MINUTE
    :returns: connection key string if successful, False otherwise
            
{
  "e": "kline",     // Event type
  "E": 123456789,   // Event time
  "s": "BNBBTC",    // Symbol
  "k": {
    "t": 123400000, // Kline start time
    "T": 123460000, // Kline close time
    "s": "BNBBTC",  // Symbol
    "i": "1m",      // Interval
    "o": "0.0010",  // Open price
    "c": "0.0020",  // Close price
    "h": "0.0025",  // High price
    "l": "0.0015",  // Low price
    "x": false,     // Is this kline closed?
  }
}
"""


# example code for starting sockets for every unique market + interval combo 
# this needs to use signalList which is the unique list of signals made from profiles.json in signalAlerts.py under the function 'buildSignalsList()'
# but idk how to get that data to this script without saving it in a file, which might be a good idea anyways
# now that i think of it im not even sure this function should be in this file... i believe in you
for market in signalList['signalAlerts']['PriceAlerts']:
    for tf in market:
        for timeframe in market["timeFrames"]:
            #if socket for this is not already started
            socket.start_kline_socket(
                symbol=market["name"], callback=call_back, interval=timeframe)





# get the API details from the environment variables
# idk how the other coder i hired gets the keys from the os environ variable but it looks cool
binance_client = Client(os.environ["BIN_KEY"], os.environ["BIN_SECRET"])
socket = BinanceSocketManager(binance_client)





# These are example commands
# fetch 1 minute klines for the last day up until now
klines = client.get_historical_klines("BNBBTC", Client.KLINE_INTERVAL_1MINUTE, "1 day ago UTC")

# fetch 30 minute klines for the last month of 2017
klines = client.get_historical_klines("ETHBTC", Client.KLINE_INTERVAL_30MINUTE, "1 Dec, 2017", "1 Jan, 2018")







#this is something that is needed for the Binance websocket functionality, the specs are in the docs, but this is copied from there
def process_message(msg):
    """ #Data example
    Websocket Errors
        If the websocket is disconnected and is unable to reconnect a message is sent to the callback to indicate this. The format is
        {
        'e': 'error',
        'm': 'Max reconnect retries reached'
        }
    """
    if msg['e'] == 'error':
        # close and restart the socket
        # maybe utilize the 'm' after a couple retries and then retart the bot?
        
        #i also found this note in the docs, unsure if needed
        ''' 
        Close and exit program
        Websockets utilise a reactor loop from the Twisted library. Using the close method above will close the websocket connections but it won't stop the reactor loop so your code may not exit when you expect.
        If you do want to exit then use the stop method from reactor like below.
        
        from twisted.internet import reactor
        # program code here
        # when you need to exit
        reactor.stop()
        '''
    else:
        # process message normally
        print("message type: {}".format(msg['e']))
        print(msg)





               



# for testing, to close the sockets after sometimes
def close_socket():
    socket.close()
    print('Interrupted')
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)


#t = Timer(60.0, close_socket) #old coder put this in for testing
#t.start()
socket.run()



