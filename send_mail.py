import os
import smtplib

# class electronic_mail:
#     def __init__(self):
#         # initialize the email server
#         pass


def send_mail(subject, msg, receiver=os.environ["GMAIL_ACC"]):
    server = smtplib.SMTP("smtp.gmail.com:587")
    server.ehlo()
    server.starttls()

    #login using susten environment variables
    server.login(os.environ["GMAIL_ACC"], os.environ["GMAIL_PASS"])

    # your messages
    message = 'Subject: {}\n\n{}'.format(subject, msg)

    # send email, from_addr, to_addrs, msg
    server.sendmail(os.environ["GMAIL_ACC"], receiver, message)

    server.quit()
    print("Success: Email sent to {}".format(receiver))

